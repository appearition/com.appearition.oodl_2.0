﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Asset.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using Appearition.Common;

namespace Appearition.ArTargetImageAndMedia
{
    /// <summary>
    /// Container of an Appearition Asset JSON ApiData model.
    /// </summary>
    [System.Serializable]
    public class Asset
    {
        //Variables
        public string assetId;
        public int productId;
        public string name;
        public int mediaGridWidth;
        public MediaFile[] mediaFiles;
        public TargetImage[] targetImages;
        public List<string> tags;

        public string createdUtcDate;
        public string createdUtcDateStr;
        public System.DateTime CreatedUtcDate => AppearitionGate.ConvertStringToDateTime(createdUtcDateStr);
        public string modifiedUtcDate;
        public string modifiedUtcDateStr;
        public System.DateTime ModifiedUtcDate => AppearitionGate.ConvertStringToDateTime(modifiedUtcDateStr);

        public Asset()
        {
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="cc">C.</param>
        public Asset(Asset cc)
        {
            assetId = cc.assetId;
            productId = cc.productId;
            name = cc.name;
            mediaGridWidth = cc.mediaGridWidth;
            cc.mediaFiles.CopyTo(mediaFiles, 0);
            cc.targetImages.CopyTo(targetImages, 0);
        }

        public bool IsImageDownloaded(int targetIndex = 0)
        {
            if (targetImages == null || targetIndex >= targetImages.Length)
                return false;

            return File.Exists(ArTargetHandler.GetPathToTargetImage(this, targetImages[targetIndex]));
        }

        public bool IsImageDownloaded(TargetImage target)
        {
            if (targetImages == null || target == null)
                return false;

            return File.Exists(ArTargetHandler.GetPathToTargetImage(this, target));
        }

        /// <summary>
        /// Whether this asset contains a tag as defined in the EMS.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public bool ContainsTag(string tag)
        {
            if (tags == null)
            {
                tags = new List<string>();
                return false;
            }

            for (int i = 0; i < tags.Count; i++)
            {
                if (tags[i].Equals(tag, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            
            return false;
        }
    }
}