// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33229,y:32719,varname:node_1873,prsc:2|emission-1749-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32551,y:32729,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1086,x:32812,y:32818,cmnt:RGB,varname:node_1086,prsc:2|A-4805-RGB,B-5983-RGB,C-5376-RGB,D-5535-OUT;n:type:ShaderForge.SFN_Color,id:5983,x:32551,y:32915,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32551,y:33079,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:33025,y:32818,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT,C-5058-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32812,y:32992,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A,D-5058-OUT;n:type:ShaderForge.SFN_Tex2d,id:8007,x:32125,y:32501,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:node_8007,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_RemapRange,id:6886,x:32320,y:32521,varname:node_6886,prsc:2,frmn:0,frmx:1,tomn:-0.1,tomx:1.1|IN-8007-R;n:type:ShaderForge.SFN_Clamp01,id:5058,x:32506,y:32521,varname:node_5058,prsc:2|IN-6886-OUT;n:type:ShaderForge.SFN_Tex2d,id:8354,x:31547,y:32618,ptovrint:False,ptlb:EyeColor,ptin:_EyeColor,varname:node_8354,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:2095,x:30842,y:32826,varname:node_2095,prsc:2,uv:0;n:type:ShaderForge.SFN_Length,id:5547,x:31668,y:33118,varname:node_5547,prsc:2|IN-9209-OUT;n:type:ShaderForge.SFN_OneMinus,id:8130,x:31836,y:33118,varname:node_8130,prsc:2|IN-5547-OUT;n:type:ShaderForge.SFN_Slider,id:8516,x:30210,y:33465,ptovrint:False,ptlb:ShadeSize,ptin:_ShadeSize,varname:node_8516,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.25,max:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9209,x:31463,y:33118,varname:node_9209,prsc:2|IN-6465-OUT,IMIN-8516-OUT,IMAX-3789-OUT,OMIN-1266-OUT,OMAX-3789-OUT;n:type:ShaderForge.SFN_Vector1,id:3789,x:31183,y:33337,varname:node_3789,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:1266,x:31183,y:33403,varname:node_1266,prsc:2,v1:-1;n:type:ShaderForge.SFN_Slider,id:9105,x:30225,y:33020,ptovrint:False,ptlb:X,ptin:_X,varname:node_9105,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:117,x:30225,y:33122,ptovrint:False,ptlb:Y,ptin:_Y,varname:node_117,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:3465,x:31061,y:33019,varname:node_3465,prsc:2|A-2095-V,B-4754-OUT;n:type:ShaderForge.SFN_Add,id:4038,x:31061,y:32903,varname:node_4038,prsc:2|A-2095-U,B-794-OUT;n:type:ShaderForge.SFN_Append,id:6465,x:31244,y:32999,varname:node_6465,prsc:2|A-4038-OUT,B-3465-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:794,x:30789,y:33058,varname:node_794,prsc:2|IN-9105-OUT,IMIN-5315-OUT,IMAX-3986-OUT,OMIN-4523-OUT,OMAX-767-OUT;n:type:ShaderForge.SFN_Vector1,id:5315,x:30382,y:33193,varname:node_5315,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:3986,x:30382,y:33251,varname:node_3986,prsc:2,v1:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4754,x:30789,y:33210,varname:node_4754,prsc:2|IN-117-OUT,IMIN-5315-OUT,IMAX-3986-OUT,OMIN-4523-OUT,OMAX-767-OUT;n:type:ShaderForge.SFN_Slider,id:3052,x:30182,y:33349,ptovrint:False,ptlb:EyeHighlightPosOffset,ptin:_EyeHighlightPosOffset,varname:node_3052,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:767,x:30572,y:33355,varname:node_767,prsc:2|A-3052-OUT,B-8516-OUT;n:type:ShaderForge.SFN_Subtract,id:4523,x:30572,y:33149,varname:node_4523,prsc:2|A-5315-OUT,B-3052-OUT;n:type:ShaderForge.SFN_Multiply,id:5604,x:31859,y:32689,varname:node_5604,prsc:2|A-8354-RGB,B-7389-OUT;n:type:ShaderForge.SFN_Slider,id:2160,x:31285,y:32808,ptovrint:False,ptlb:StartColorFade,ptin:_StartColorFade,varname:node_2160,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_OneMinus,id:7389,x:31665,y:32793,varname:node_7389,prsc:2|IN-2160-OUT;n:type:ShaderForge.SFN_Add,id:5535,x:32348,y:32862,varname:node_5535,prsc:2|A-5604-OUT,B-5332-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:5784,x:32122,y:33226,varname:node_5784,prsc:2|IN-8130-OUT,IMIN-3402-OUT,IMAX-2937-OUT,OMIN-3402-OUT,OMAX-1185-OUT;n:type:ShaderForge.SFN_Vector1,id:2937,x:31836,y:33292,varname:node_2937,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:3402,x:31836,y:33242,varname:node_3402,prsc:2,v1:0;n:type:ShaderForge.SFN_Slider,id:1185,x:31679,y:33372,ptovrint:False,ptlb:MaxHighlight,ptin:_MaxHighlight,varname:node_1185,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Clamp,id:5332,x:32086,y:33075,varname:node_5332,prsc:2|IN-8130-OUT,MIN-3402-OUT,MAX-1185-OUT;proporder:4805-8354-8007-5983-8516-2160-1185-3052-9105-117;pass:END;sub:END;*/

Shader "HiziProjects/oodl_eye_rotation" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _EyeColor ("EyeColor", 2D) = "white" {}
        _Mask ("Mask", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _ShadeSize ("ShadeSize", Range(0, 1)) = 0.25
        _StartColorFade ("StartColorFade", Range(0, 1)) = 0.2
        _MaxHighlight ("MaxHighlight", Range(0, 1)) = 1
        _EyeHighlightPosOffset ("EyeHighlightPosOffset", Range(-1, 1)) = 0
        _X ("X", Range(0, 1)) = 0
        _Y ("Y", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _EyeColor; uniform float4 _EyeColor_ST;
            uniform float _ShadeSize;
            uniform float _X;
            uniform float _Y;
            uniform float _EyeHighlightPosOffset;
            uniform float _StartColorFade;
            uniform float _MaxHighlight;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 _EyeColor_var = tex2D(_EyeColor,TRANSFORM_TEX(i.uv0, _EyeColor));
                float node_5315 = 0.0;
                float node_3986 = 1.0;
                float node_4523 = (node_5315-_EyeHighlightPosOffset);
                float node_767 = (_EyeHighlightPosOffset+_ShadeSize);
                float node_3789 = 1.0;
                float node_1266 = (-1.0);
                float node_8130 = (1.0 - length((node_1266 + ( (float2((i.uv0.r+(node_4523 + ( (_X - node_5315) * (node_767 - node_4523) ) / (node_3986 - node_5315))),(i.uv0.g+(node_4523 + ( (_Y - node_5315) * (node_767 - node_4523) ) / (node_3986 - node_5315)))) - _ShadeSize) * (node_3789 - node_1266) ) / (node_3789 - _ShadeSize))));
                float node_3402 = 0.0;
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float node_5058 = saturate((_Mask_var.r*1.2+-0.1));
                float node_603 = (_MainTex_var.a*_Color.a*i.vertexColor.a*node_5058); // A
                float3 emissive = ((_MainTex_var.rgb*_Color.rgb*i.vertexColor.rgb*((_EyeColor_var.rgb*(1.0 - _StartColorFade))+clamp(node_8130,node_3402,_MaxHighlight)))*node_603*node_5058);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
