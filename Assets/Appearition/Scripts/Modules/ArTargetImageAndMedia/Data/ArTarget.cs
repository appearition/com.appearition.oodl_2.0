﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

namespace Appearition.ArTargetImageAndMedia
{
    /// <summary>
    /// Container of an Appearition Asset JSON ApiData model.
    /// </summary>
    [System.Serializable]
    public class ArTarget : Asset
    {
        //Variables 
        public int arTargetId;
        public bool isPublished;

        public ArTarget()
        {
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="cc">C.</param>
        public ArTarget(ArTarget cc) : base(cc)
        {
            arTargetId = cc.arTargetId;
            isPublished = cc.isPublished;
        }
    }
}