﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: MediaFile.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

namespace Appearition.Common
{
    /// <summary>
    /// Container of an Appearition Media File JSON ApiData model.
    /// </summary>
    [System.Serializable]
    public class MediaFile
    {
        //Variables
        public int arMediaId;
        public string animationName;
        public string checksum;
        public string custom;
        public string fileName;
        public bool isAutoPlay;
        public bool isInteractive;
        public bool isPreDownload;
        public bool isPrivate;
        public bool isTracking;
        public string language;
        public string lastModified;
        public int mediaGridOffset;
        public string mediaType;
        public string mimeType;
        public int resolution;
        public float rotationX;
        public float rotationY;
        public float rotationZ;
        public float scaleX;
        public float scaleY;
        public float scaleZ;
        public string text;
        public float translationX;
        public float translationY;
        public float translationZ;
        public string url;

        public MediaFile()
        {
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="cc">C.</param>
        public MediaFile(MediaFile cc)
        {
            arMediaId = cc.arMediaId;
            animationName = cc.animationName;
            checksum = cc.checksum;
            custom = cc.custom;
            fileName = cc.fileName;
            isAutoPlay = cc.isAutoPlay;
            isInteractive = cc.isInteractive;
            isPreDownload = cc.isPreDownload;
            isPrivate = cc.isPrivate;
            isTracking = cc.isTracking;
            language = cc.language;
            lastModified = cc.lastModified;
            mediaGridOffset = cc.mediaGridOffset;
            mediaType = cc.mediaType;
            mimeType = cc.mimeType;
            resolution = cc.resolution;
            rotationX = cc.rotationX;
            rotationY = cc.rotationY;
            rotationZ = cc.rotationZ;
            scaleX = cc.scaleX;
            scaleY = cc.scaleY;
            scaleZ = cc.scaleZ;
            text = cc.text;
            translationX = cc.translationX;
            translationY = cc.translationY;
            translationZ = cc.translationZ;
            url = cc.url;
        }
    }
}