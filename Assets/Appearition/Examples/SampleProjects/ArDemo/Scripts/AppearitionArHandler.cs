﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using Appearition.Tenant;
using UnityEngine.Events;

namespace Appearition.Example
{
    /// <summary>
    /// Main handle which controls anything related to the AR implementation.
    /// It contains all events, states and settings related to the Ar implementation.
    /// </summary>
    [RequireComponent(typeof(IArProviderHandler))]
    public class AppearitionArHandler : MonoBehaviour
    {
        #region Enums

        /// <summary>
        /// Contains the different possible states for an Target being tracked.
        /// </summary>
        public enum TargetState
        {
            None = -1,
            TargetFound,
            TargetLost,
            Unknown
        }

        #endregion

        #region Events

        public delegate void InitializationComplete();

        /// <summary>
        /// Occurs whenever both Vuforia and the AppearitionArHandler are done initializing, and ready to do some AR.
        /// </summary>
        public static event InitializationComplete OnInitializationComplete;

        public delegate void ScanStateChanged(bool isScanning);

        /// <summary>
        /// Called anytime the scan value has been changed.
        /// </summary>
        public static event ScanStateChanged OnScanStateChanged;


        public delegate void TargetStateChanged(AppearitionExperience arAsset, TargetState newState);

        /// <summary>
        /// Event triggered whenever the given target has been 
        /// </summary>
        public static event TargetStateChanged OnTargetStateChanged;

        #endregion

        #region Singleton

        private static AppearitionArHandler _instance;

        /// <summary>
        /// Singleton of the Appearition ArHandler.
        /// </summary>
        public static AppearitionArHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<AppearitionArHandler>();

                    if (_instance == null)
                        _instance = new GameObject("Appearition Ar Handler").AddComponent<AppearitionArHandler>();
                }

                return _instance;
            }
        }

        #endregion

        //public GameObject countdown;

        //References
        private IArProviderHandler _providerHandler;

        /// <summary>
        /// Locates the current Ar Provider handler attached on this object.
        /// </summary>
        public IArProviderHandler ProviderHandler
        {
            get
            {
                if (_providerHandler == null)
                {
                    //HACK for now, just use ArFoundationHandler
                    //_providerHandler = GetComponent<IArProviderHandler>();
                    _providerHandler = FindObjectOfType<VuforiaCloudProviderHandler>();
                }

                return _providerHandler;
            }
            protected set
            {
                _providerHandler = value;
                AppearitionLogger.LogDebug("Ar Provider has been set: " + _providerHandler?.GetType().Name);
            }
        }

        /// <summary>
        /// Returns whether or not the current ArProvider is currently scanning and looking for targets.
        /// </summary>
        /// <value><c>true</c> if is provider scanning; otherwise, <c>false</c>.</value>
        public bool IsProviderScanning
        {
            get { return ProviderHandler != null && ProviderHandler.IsScanning; }
        }

        /// <summary>
        /// Returns the current state of the target being tracked, if any.
        /// </summary>
        /// <value>The state of the current target.</value>
        public TargetState CurrentTargetState { get; private set; }

        //Internal Variables
        [Tooltip("Whether all fullscreen medias should have a black transparent background behind them.")]
        public bool shouldFullscreenMediaHaveBlackBackground = true;
        public bool enableArProviderOnceInitializationComplete = true;

        /// <summary>
        /// Defines whether or not the ArProvider is currently initialized or not.
        /// </summary>
        public bool IsArProviderInitialized { get; protected set; }

        private bool _isInitialized;

        /// <summary>
        /// Whether or not the Appearition Ar Handler's initialization is complete.
        /// Do note that it will not complete until Vuforia's Initialization is also complete.
        /// </summary>
        public bool IsInitialized
        {
            get { return _isInitialized; }
            protected set
            {
                bool previousValue = _isInitialized;

                _isInitialized = value;

                if (!previousValue && _isInitialized)
                {
                    if (OnInitializationComplete != null)
                        OnInitializationComplete();
                }
            }
        }

        //Storage ApiData
        /// <summary>
        /// Contains all the assets from the attached EMS.
        /// </summary>
        public List<Asset> CurrentDataRaw { get; private set; }

        void Awake()
        {
            //Initialize storages
            CurrentDataRaw = new List<Asset>();
        }

        IEnumerator Start()
        {
            Debug.Log(Time.realtimeSinceStartup);
            //Get license first
            yield return ProviderHandler.SetupLicense();

            Debug.Log(Time.realtimeSinceStartup + "1");
            //Launch the Asset fetching request
            bool? hasRequestFinished = null;

            if (enableArProviderOnceInitializationComplete)
                ProviderHandler.ChangeArProviderActiveState(true);
            //countdown.SetActive(true);
            ArTargetHandler.GetChannelExperiences(CurrentDataRaw, false, false, true, onComplete: isSuccess => { hasRequestFinished = isSuccess; });

            while (!hasRequestFinished.HasValue)
                yield return null;

            Debug.Log(Time.realtimeSinceStartup + "2");
            if (hasRequestFinished.Value)
            {
                AppearitionLogger.LogInfo(string.Format("Assets successfully fetched from the EMS! Got {0} Assets to use within this channel.", CurrentDataRaw.Count));
            }
            else
                AppearitionLogger.LogError("An error occured when trying to fetch the assets from the EMS, and no ApiData is available.");

            IsInitialized = true;

            //Activate the Provider and let it do scanning.
            //countdown.SetActive(false);
            Debug.Log(Time.realtimeSinceStartup + "3");
        }

        private void EmsDataContainer_OnAssetsRefreshed(List<ArTarget> artargets, List<Asset> assets)
        {
            RefreshAugmentableAssets(assets);
        }

        #region Experience Handling 

        /// <summary>
        /// Initializes a single target using the AssetId of said target.
        /// </summary>
        /// <param name="assetId"></param>
        public static AppearitionExperience CreateExperienceFromAsset(string assetId)
        {
            if (string.IsNullOrEmpty(assetId))
                return null;

            return CreateExperienceFromAsset(Instance.CurrentDataRaw.Find(o => o.assetId.Equals(assetId)));
        }

        /// <summary>
        /// Initializes a single target using the Asset container of said target.
        /// </summary>
        /// <param name="asset"></param>
        public static AppearitionExperience CreateExperienceFromAsset(Asset asset)
        {
            if (asset == null)
                return null;

            var tmpExperience = new GameObject(string.Format("Experience Name: {0}, Id: {1}", asset.name, asset.assetId)).AddComponent<AppearitionExperience>();
            tmpExperience.transform.SetParent(Instance.transform);

            //Init it
            tmpExperience.SetupExperience(asset);

            return tmpExperience;
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Informs the AppearitionArHandler about the current initialization state of the ArProvider.
        /// </summary>
        /// <param name="newValue"></param>
        public void ArProviderInitializationStateChanged(bool newValue)
        {
            IsArProviderInitialized = newValue;
        }

        /// <summary>
        /// Called only by ArProviers. Handles the OnScanStateChanged event.
        /// </summary>
        /// <param name="newValue"></param>
        public void ArProviderScanStateChanged(bool newValue)
        {
            //Clear current target being scanned. Good 8AR-like applications.
            CurrentTargetState = TargetState.None;

            if (OnScanStateChanged != null)
                OnScanStateChanged(newValue);
        }

        /// <summary>
        /// Should be called by the ArProvider whenever an ArTarget has been found or lost. Handles the events.
        /// </summary>
        /// <param name="experienceTracked"></param>
        /// <param name="newState"></param>
        public void ArProviderTargetStateChanged(AppearitionExperience experienceTracked, TargetState newState)
        {
            //Retardation test
            CurrentTargetState = newState;

            if (OnTargetStateChanged != null)
                OnTargetStateChanged(experienceTracked, newState);
        }

        #endregion

        #region Ar Provider Handling

        /// <summary>
        /// Called by the script in charge of refreshing and storing all the assets which the user can use.
        /// Refreshes the library of the provider if it has offline capability (non-cloud reco).
        /// </summary>
        /// <param name="assets"></param>
        public void RefreshAugmentableAssets(List<Asset> assets)
        {
            if (ProviderHandler is IOfflineMarkerArProviderHandler handler)
                handler.UpdateTrackableAssetCollection(assets);
        }

        public void ChangeArProviderActiveState(bool desiredState)
        {
            if (ProviderHandler != null)
                ProviderHandler.ChangeArProviderActiveState(desiredState);
        }

        /// <summary>
        /// Changes the scanning state of the current Ar Provider, if setup properly..
        /// </summary>
        /// <param name="desiredState"></param>
        public void ChangeArProviderScanningState(bool desiredState)
        {
            if (ProviderHandler != null)
                ProviderHandler.ChangeScanningState(desiredState);
        }

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        public void ClearCurrentTargetBeingTracked(bool doScanAgain = true)
        {
            if (ProviderHandler != null)
                ProviderHandler.ClearTargetsBeingTracked(doScanAgain);
        }

        #endregion
    }
}