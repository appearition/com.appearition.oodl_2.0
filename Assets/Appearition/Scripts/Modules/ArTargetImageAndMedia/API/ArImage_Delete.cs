﻿using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    public class ArImage_Delete : BaseApiPost
    {
        //Variables
        public string Data;

        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public int arImageId;
        }
    }
}