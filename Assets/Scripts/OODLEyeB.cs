﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OODLEyeB : MonoBehaviour
{
	//References
	Material eyeMat;

	//Constants
	const float CENTER = 0.5f;

	//Internal Variables
	int xHash;
	int yHash;
	public float movSpeed = 3;
	float x, y;
	public float yOffset = Mathf.PI / 2;
	public float waveAmplitude = 0.5f;

	void Awake ()
	{
		eyeMat = this.transform.GetComponent<Image> ().material;

		xHash = Shader.PropertyToID ("_X");
		yHash = Shader.PropertyToID ("_Y");
	}

	void Update ()
	{
		x += Time.deltaTime * movSpeed;
		y += Time.deltaTime * movSpeed;

		eyeMat.SetFloat (xHash, CENTER + Mathf.Cos (x) * waveAmplitude);
		eyeMat.SetFloat (yHash, CENTER + Mathf.Cos (y + yOffset) * waveAmplitude);
	}

}
