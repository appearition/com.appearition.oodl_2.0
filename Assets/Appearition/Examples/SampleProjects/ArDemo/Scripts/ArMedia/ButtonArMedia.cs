using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    public class ButtonArMedia : BaseCanvasArMedia
    {
        /// <summary>
        /// Image displaying the content of the media.
        /// </summary>
        protected Image ImageRef { get; set; }

        private bool? _isImageDownload;

        public override bool IsMediaReadyAndDownloaded => _isImageDownload.GetValueOrDefault();
        DeviceOrientation _currentOrientation;

        public override void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            base.Setup(associatedExperience, media);
            Data.isTracking = false;

            _isImageDownload = null;

            //Create an image object as a child, which will contain the image itself.
            ImageRef = new GameObject("Image").AddComponent<Image>();
            ImageRef.transform.SetParent(transform);

            ImageRef.type = Image.Type.Simple;
            ImageRef.preserveAspect = !Data.isTracking;


            if (!string.IsNullOrEmpty(Data.text))
            {
            Button attachedButton = ImageRef.gameObject.AddComponent<Button>();
            attachedButton.onClick.AddListener(OnAttachedButtonPressed);
            }

            UpdateButtonSize();
            //Position the image
            //SetMediaRectPosition(ImageRef.rectTransform, Data.isTracking);

            Debug.Log(Data.text);
            Debug.Log(Data.url);
        }

        void UpdateButtonSize()
        {
            if (ImageRef == null)
                return;

            if (Input.deviceOrientation == DeviceOrientation.Portrait ||
                Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown ||
                Input.deviceOrientation == DeviceOrientation.Unknown)
            {
                ImageRef.rectTransform.anchorMin = new Vector2(0, 0);
                ImageRef.rectTransform.anchorMax = new Vector2(1, 1);
                ImageRef.rectTransform.pivot = new Vector2(0.5f, 0f);
                //ImageRef.rectTransform.position = new Vector3(0, 300, 0);
                ImageRef.rectTransform.offsetMin = new Vector2(0, 300);
                ImageRef.rectTransform.offsetMax = new Vector2(0, 650 - Screen.height);
            }
            else
            {
                ImageRef.rectTransform.anchorMin = new Vector2(0.5f, 0);
                ImageRef.rectTransform.anchorMax = new Vector2(0.5f, 0);
                ImageRef.rectTransform.pivot = new Vector2(0.5f, 0f);
                //ImageRef.rectTransform.position = new Vector3(0, 300, 0);
                ImageRef.rectTransform.anchoredPosition = new Vector2(0,220);
                ImageRef.rectTransform.sizeDelta = new Vector2(1200,200);
            }
            _currentOrientation = Input.deviceOrientation;
        }

        private void Update()
        {
            if (_currentOrientation != Input.deviceOrientation)
                UpdateButtonSize();
        }

        public override void ChangeDisplayState(bool state)
        {
            //If loaded but no sprite is present, download / load the file!
            if (state && ImageRef.sprite == null && !_isImageDownload.HasValue)
            {
                //No sprite? Download it. While it's loading, use a placeholder.
                Sprite placeholderSprite = AppearitionArDemoConstants.GetImagePlaceholder();

                if (placeholderSprite != null)
                    ImageRef.sprite = placeholderSprite;

                ArTargetHandler.LoadMediaFileContent(ExperienceRef.Data, Data, mediaOutcome =>
                {
                    if (mediaOutcome == null)
                        Debug.Log("Oh no, no sprite =(");


                    if (mediaOutcome != null)
                    {
                        Sprite tmpSprite = ImageUtility.LoadOrCreateSprite(mediaOutcome);
                        if (tmpSprite != null)
                            ImageRef.sprite = tmpSprite;
                        else
                            ImageRef.sprite = null;
                    }
                    else
                        ImageRef.sprite = null;

                    //Flag!
                    _isImageDownload = ImageRef.sprite != null;
                });
            }

            base.ChangeDisplayState(state);
        }

        /// <summary>
        /// Called when clicking on the Image, if the MediaFile contains a URL in the Text field.
        /// </summary>
        void OnAttachedButtonPressed()
        {
                Debug.Log(Data.text);
                Application.OpenURL(Data.text);           
        }
    }
}