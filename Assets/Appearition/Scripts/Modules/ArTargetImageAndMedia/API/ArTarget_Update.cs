// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_Update.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ArTarget/Update/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class ArTarget_Update : BaseApiPost
    {
        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public int arTargetId;
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData
        {
        }

        /// <summary>
        /// Post ApiData
        /// </summary>
        public class PostData
        {
            public string name;
        }
    }
}