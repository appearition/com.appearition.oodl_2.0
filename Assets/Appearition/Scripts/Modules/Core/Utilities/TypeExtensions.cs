﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: TypeExtensions.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Appearition.Common.TypeExtensions
{
    public static class TypeExtension
    {
        /// <summary>
        /// Returns the given int as a float
        /// </summary>
        /// <returns>The float.</returns>
        /// <param name="tmp">tmp.</param>
        public static float ToFloat(this int tmp)
        {
            return tmp;
        }

        /// <summary>
        /// Returns the given float as an int
        /// </summary>
        /// <returns>The int.</returns>
        /// <param name="tmp">tmp.</param>
        public static int ToInt(this float tmp)
        {
            return (int) tmp;
        }

        public static void DestroySelf(this GameObject tmp)
        {
            UnityEngine.Object.Destroy(tmp);
        }

        /// <summary>
        /// Removes the duplicates from a given list.
        /// </summary>
        /// <returns>The duplicates.</returns>
        /// <param name="items">Items.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void RemoveDuplicates<T>(this List<T> items)
        {
            List<T> result = new List<T>();
            for (int i = 0; i < items.Count; i++)
            {
                // Assume not duplicate.
                bool duplicate = false;
                for (int z = 0; z < i; z++)
                {
                    if (items[z].Equals(items[i]))
                    {
                        // This is a duplicate.
                        duplicate = true;
                        break;
                    }
                }

                // If not duplicate, add to result.
                if (!duplicate)
                {
                    result.Add(items[i]);
                }
            }

            items.Clear();
            items.AddRange(result);
        }


        #region String Extensions

        /// <summary>
        /// Deserializes a comma separated value (CSV) as a string array.
        /// </summary>
        /// <returns>The CS.</returns>
        /// <param name="tmp">Tmp.</param>
        /// <param name="removeNullOrEmpty">If set to <c>true</c> remove null or empty.</param>
        public static string[] DeserializeCSV(this string tmp, bool removeNullOrEmpty = true)
        {
            string regex = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
            string[] deserializedCSV = System.Text.RegularExpressions.Regex.Split
                (tmp, regex);

            if (removeNullOrEmpty)
                deserializedCSV = deserializedCSV.Where(o => !string.IsNullOrEmpty(o)).ToArray();
            return deserializedCSV;
        }

        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null:
                case "":
                    throw new ArgumentException("The input cannot be null or empty cannot be empty");
                default:
                    return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        #endregion
    }
}

namespace Appearition.Common.ObjectExtensions
{
    public static class ObjectExtension
    {
        #region Object And Stream Extensions

        /// <summary>
        /// Converts a Stream to a byte array.
        /// </summary>
        /// <returns>The byte array.</returns>
        /// <param name="stream">Stream.</param>
        public static byte[] ToByteArray(this System.IO.Stream stream)
        {
            byte[] output = new byte[stream.Length];
            for (int bytesCopied = 0; bytesCopied < stream.Length;)
                bytesCopied += stream.Read(output, bytesCopied, Convert.ToInt32(stream.Length) - bytesCopied);
            return output;
        }

        /// <summary>
        /// Deserializes a binary stream to an object.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="data"></param>
        public static object ToObject(this byte[] data)
        {
            //Retardation test
            if (data == null)
                return null;

            System.Runtime.Serialization.IFormatter formatter =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.MemoryStream stream = new System.IO.MemoryStream(data);
            return formatter.Deserialize(stream);
        }

        /// <summary>
        /// Serializes an object to a binary stream.
        /// </summary>
        /// <returns>The stream.</returns>
        /// <param name="obj">Object.</param>
        public static System.IO.Stream ToStream(this object obj)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            System.Runtime.Serialization.IFormatter formatter =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream;
        }

        public static string Checksum(this Stream stream)
        {
            string checksum;

            using (var md5 = MD5.Create())
            {
                checksum = BitConverter.ToString(md5.ComputeHash(stream));
            }

            return checksum.Replace("-", string.Empty);
        }

        #endregion
    }
}