﻿using Appearition.Common;

namespace Appearition.Example
{
    /// <summary>
    /// Template of an ArMedia class, handling the way a single MediaType should behave.
    /// </summary>
    public interface IArMedia
    {
        /// <summary>
        /// Initializes the MediaType.
        /// </summary>
        /// <param name="associatedExperience"></param>
        /// <param name="media"></param>
        void Setup(AppearitionExperience associatedExperience, MediaFile media);
        
        //The media data.
        MediaFile Data { get; }

        /// <summary>
        /// Changes the display state of the given Media.
        /// </summary>
        /// <param name="state"></param>
        void ChangeDisplayState(bool state);

        /// <summary>
        /// Whether the current media is being shown or not.
        /// </summary>
        bool IsVisible { get; }

        void OnExperienceTrackingStateChanged(bool isTracking);
        
        bool IsMediaReadyAndDownloaded { get; }
    }
}