﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelsToSwitch : MonoBehaviour
{

    public List<GameObject> panelToActivate = new List<GameObject>();
    public List<GameObject> panelToDeactivate = new List<GameObject>();


    public void SwitchPanels()
    {
        TurnPanelsOn();
        TurnPanelsOff();

    }

    public void TurnPanelsOn()
    {
        foreach (GameObject pta in panelToActivate)
        {
            if (pta != null)
                pta.SetActive(true);
        }

    }

    public void TurnPanelsOff()
    {
        foreach (GameObject ptda in panelToDeactivate)
        {
            if (ptda != null)
                ptda.SetActive(false);
        }
    }
}
