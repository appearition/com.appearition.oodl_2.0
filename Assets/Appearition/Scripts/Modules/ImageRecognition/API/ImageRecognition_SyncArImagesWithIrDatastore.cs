﻿using Appearition.API;

namespace Appearition.ImageRecognition
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ImageRecognition/SyncArImagesWithIrDatastore/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class ImageRecognition_SyncArImagesWithIrDatastore : BaseApiPost
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        public override AuthenticationOverrideType AuthenticationOverride
        {
            get { return AuthenticationOverrideType.ApplicationToken; }
        }

        //Variables
        public string Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData
        {
            public int channelId;
            public string provider;
            
            public PostData(DataStore cc)
            {
                channelId = cc.channelId;
                provider = cc.provider;
            }
        }
    }
}