﻿#pragma warning disable 0649

using System.Collections;
using System.Linq;
using Vuforia;
using UnityEngine;

namespace Appearition.Example
{
    [RequireComponent(typeof(CloudRecoBehaviour))]
    public class VuforiaCloudProviderHandler : MonoBehaviour, IArProviderHandler
    {
        /// <summary>
        /// Timer keeping track of when the Scanning mode should be disabled, if feature toggled in the AppearitionArHandler.
        /// </summary>
        float _disableScanTimer;

        //Handy Properties
        private CloudRecoBehaviour _cloudRecoB;

        public CloudRecoBehaviour CloudRecoB
        {
            get
            {
                if (_cloudRecoB == null)
                    _cloudRecoB
                        = GetComponent<CloudRecoBehaviour>();
                return _cloudRecoB;
            }
        }


        private ObjectTracker _objectTracker;

        public ObjectTracker ObjectTracker
        {
            get { return _objectTracker ?? (_objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>()); }
        }

        private ImageTargetFinder _currentTargetFinder;

        public ImageTargetFinder CurrentTargetFinder
        {
            get
            {
                if (_currentTargetFinder == null)
                    _currentTargetFinder = (ImageTargetFinder) ObjectTracker?.GetTargetFinder<ImageTargetFinder>();
                return _currentTargetFinder;
            }
            private set => _currentTargetFinder = value;
        }

        private bool _isScanning;

        /// <summary>
        /// Whether Vuforia is currently scanning a target or not.
        /// </summary>
        public bool IsScanning
        {
            get { return _isScanning; }
            protected set
            {
                _isScanning = value;

                ////Tell the AppearitionArHandler about it!
                AppearitionArHandler.Instance.ArProviderScanStateChanged(_isScanning);
            }
        }

        private bool _isArProviderInitialized;

        /// <summary>
        /// Whether Vuforia, along with its relevant components, is initialized.
        /// </summary>
        public bool IsArProviderInitialized
        {
            get
            {
                bool oldValue
                    = _isArProviderInitialized;

                //Check the new value
                _isArProviderInitialized = VuforiaManager.Instance.Initialized;

                if (oldValue != _isArProviderInitialized)
                    AppearitionArHandler.Instance.ArProviderInitializationStateChanged(_isArProviderInitialized);

                return _isArProviderInitialized;
            }
        }

        /// <summary>
        /// Whether Vuforia's engine is active or not.
        /// </summary>
        public bool IsArProviderActive
        {
            get { return VuforiaBehaviour.Instance != null && VuforiaBehaviour.Instance.enabled; }
        }

        Camera _providerCamera;

        public Camera ProviderCamera
        {
            get
            {
                if (_providerCamera == null)
                    _providerCamera = VuforiaBehaviour.Instance.GetComponent<Camera>();
                return _providerCamera;
            }
        }

        void Awake()
        {
            CloudRecoB.RegisterOnInitializedEventHandler(OnInitialized);
            CloudRecoB.RegisterOnInitErrorEventHandler(OnInitError);
            CloudRecoB.RegisterOnNewSearchResultEventHandler(OnNewSearchResult);
            CloudRecoB.RegisterOnStateChangedEventHandler(OnStateChanged);
            CloudRecoB.RegisterOnUpdateErrorEventHandler(OnUpdateError);
        }

        public void InitializeVuforia()
        {
            VuforiaBehaviour.Instance.enabled = true;
            VuforiaRuntime.Instance.InitVuforia();
        }

        #region Init 

        public void OnInitError(TargetFinder.InitState initError)
        {
            Debug.LogError("Init error: " + initError);
        }

        /// <summary>
        /// Occurs when the tracker is initialized.
        /// </summary>
        /// <param name="targetFinder">Target finder.</param>
        public void OnInitialized(TargetFinder targetFinder)
        {
            Debug.Log("Cloud Handler initialized !");
            CurrentTargetFinder = targetFinder as ImageTargetFinder;
//            AppearitionArHandler.Instance.ArProviderInitializationStateChanged(true);
//           ChangeScanningState(AppearitionArHandler.Instance.IsInitialized);
            ChangeScanningState(true);
        }

        /// <summary>
        /// Step only required if the cloud reco was initialized, then de-initialized using DeInitTargetFinder to swap the credentials.
        /// </summary>
        public void InitTargetFinder()
        {
            if (CurrentTargetFinder != null &&
                CurrentTargetFinder.GetInitState() != TargetFinder.InitState.INIT_SUCCESS &&
                CurrentTargetFinder.GetInitState() != TargetFinder.InitState.INIT_RUNNING)
                CurrentTargetFinder.StartInit(CloudRecoB.AccessKey, CloudRecoB.SecretKey);
        }

        /// <summary>
        /// Step required before changing the Cloud reco credentials.
        /// </summary>
        /// <returns></returns>
        public bool DeInitTargetFinder()
        {
            if (CurrentTargetFinder != null && CurrentTargetFinder.GetInitState() == TargetFinder.InitState.INIT_SUCCESS)
            {
                CurrentTargetFinder.Deinit();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the license set up for Vuforia.
        /// </summary>
        /// <returns></returns>
        public IEnumerator SetupLicense()
        {
            yield return null;
         //   yield return VuforiaLicenseFetcherB.FetchVuforiaLicense();
        }

        #endregion


        #region Event Handling

        public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
        {
            TargetFinder.CloudRecoSearchResult cloudResult
                = (TargetFinder.CloudRecoSearchResult) targetSearchResult;
            
            AppearitionLogger.LogInfo(string.Format("Target found, AssetID:{0}", cloudResult.MetaData));

            AppearitionExperience targetObject
                //= AppearitionArHandler.Instance.InitializeSingleTarget(cloudResult.MetaData);
                = AppearitionArHandler.CreateExperienceFromAsset(cloudResult.MetaData);

            if (targetObject != null && CurrentTargetFinder != null)
            {
                //Start tracking, and add an event handler if none exist.
                ImageTargetBehaviour imageTarget
                    = (ImageTargetBehaviour) CurrentTargetFinder.EnableTracking(targetSearchResult, targetObject.gameObject);

                VuforiaTargetEventHandler eventHandler = targetObject.GetComponent<VuforiaTargetEventHandler>();

                if (eventHandler == null)
                    targetObject.gameObject.AddComponent<VuforiaTargetEventHandler>().Setup(targetObject, imageTarget);
                else
                    eventHandler.Setup(targetObject, imageTarget);

                //Disable tracking on target found.
                if (AppearitionArDemoConstants.DISABLE_TRACKING_ON_TARGET_FOUND)
                    ChangeScanningState(false);
            }
            else
            {
                AppearitionLogger.LogWarning(string.Format("Unable to find the experience linked to the target of id {0}",
                    cloudResult.MetaData));
            }
        }

        public void OnStateChanged(bool scanning)
        {
            if (VuforiaManager.Instance != null && IsArProviderInitialized)
                AppearitionLogger.LogInfo("Scan state changed to : " + scanning);
            IsScanning = scanning;
        }

        public void OnUpdateError(TargetFinder.UpdateState updateError)
        {
        }

        #endregion

        /// <summary>
        /// Initializes or de-initializes the ArProvider.
        /// </summary>
        /// <param name="shouldBeEnabled"></param>
        public void ChangeArProviderActiveState(bool shouldBeEnabled)
        {
            if (VuforiaManager.Instance != null)
            {
                if (!VuforiaManager.Instance.Initialized)
                    InitializeVuforia();
                else if (VuforiaBehaviour.Instance.enabled != shouldBeEnabled && VuforiaManager.Instance.Initialized)
                {
                    VuforiaBehaviour.Instance.enabled = shouldBeEnabled;
                    
                    if(!shouldBeEnabled)
                        ClearTargetsBeingTracked(false);
                }
            }
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        public void ChangeScanningState(bool shouldScan)
        {
            ChangeScanningState(shouldScan, AppearitionArDemoConstants.CLEAR_TRACKER_ON_TARGET_LOST && shouldScan);
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        /// <param name="clearTracker"></param>
        public void ChangeScanningState(bool shouldScan, bool clearTracker)
        {
            if (CurrentTargetFinder == null)
                return;

            if (shouldScan)
                CurrentTargetFinder.StartRecognition();
            else
                CurrentTargetFinder.Stop();

            if (clearTracker)
                CurrentTargetFinder.ClearTrackables(AppearitionArDemoConstants.USING_CLOUD_RECO);

            IsScanning = shouldScan;
        }

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        /// <param name="shouldRestartScanning">If set to <c>true</c> should restart scanning.</param>
        public void ClearTargetsBeingTracked(bool shouldRestartScanning = true)
        {
            if (CurrentTargetFinder != null)
                CurrentTargetFinder.ClearTrackables();

            if (shouldRestartScanning)
                ChangeScanningState(true, false);

            //Let the ArHandler know about the new Object's state.
            AppearitionArHandler.Instance.ArProviderTargetStateChanged(null, AppearitionArHandler.TargetState.None);
        }

        public void RefocusCamera()
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }
}