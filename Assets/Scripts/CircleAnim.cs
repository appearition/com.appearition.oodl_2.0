﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleAnim : MonoBehaviour
{
	public Image outerRingImage;
	public Image innerRingImage;
	public float ringAnimSpeed = 1;
	public float innerRingSpeedMulti = 1.75f;
	public float ringRotationSpeed = 10;
	float innerRingSwapTimer = 0;
	float outerRingSwapTimer = 0;
	public float swapDelay = 0.4f;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		innerRingImage.fillAmount = Mathf.Abs(Mathf.Sin(Time.time * ringAnimSpeed * innerRingSpeedMulti));
		outerRingImage.fillAmount = Mathf.Abs(Mathf.Sin(Time.time * ringAnimSpeed));

		if ((innerRingImage.fillAmount < 0.1f || innerRingImage.fillAmount > 0.9f) && innerRingSwapTimer <= 0)
		{
			innerRingImage.fillClockwise = !innerRingImage.fillClockwise;
			innerRingSwapTimer = swapDelay / innerRingSpeedMulti;
		}

		if ((outerRingImage.fillAmount < 0.1f || outerRingImage.fillAmount > 0.9f) && outerRingSwapTimer <= 0)
		{
			outerRingImage.fillClockwise = !outerRingImage.fillClockwise;
			outerRingSwapTimer = swapDelay;
		}
		innerRingImage.transform.Rotate(Vector3.forward * ringRotationSpeed * innerRingSpeedMulti * Time.deltaTime);
		outerRingImage.transform.Rotate(Vector3.forward * ringRotationSpeed * Time.deltaTime);

		innerRingSwapTimer -= Time.deltaTime;
		outerRingSwapTimer -= Time.deltaTime;
	}
}
