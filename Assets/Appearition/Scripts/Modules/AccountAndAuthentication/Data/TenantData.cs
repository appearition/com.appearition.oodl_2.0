﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Tenant.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

namespace Appearition.AccountAndAuthentication
{
    /// <summary>
    /// Basic information a Tenant as on the EMS contains.
    /// </summary>
    [System.Serializable]
    public class TenantData
    {
        public string tenantKey;
        public string tenantName;
    }
}