// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_List.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ArTarget/List/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class ArTarget_List : BaseApiGet
    {
        //Variables
        public ArTarget[] Data;
    }
}