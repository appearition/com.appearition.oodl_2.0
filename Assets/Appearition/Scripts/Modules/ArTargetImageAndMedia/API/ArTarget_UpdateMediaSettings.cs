// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_UpdateMediaSettings.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;
using Appearition.Common;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ArTarget/UpdateMediaSettings/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class ArTarget_UpdateMediaSettings : BaseApiPost
    {
        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public int arTargetId;
            public int arMediaId;
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData
        {
        }

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData : MediaFile
        {
            public PostData(MediaFile cc) : base(cc)
            {
                //arMediaId = cc.arMediaId;
                //animationName = cc.animationName;
                //checksum = cc.checksum;
                //custom = cc.custom;
                //fileName = cc.fileName;
                //isAutoPlay = cc.isAutoPlay;
                //isInteractive = cc.isInteractive;
                //isPreDownload = cc.isPreDownload;
                //isPrivate = cc.isPrivate;
                //isTracking = cc.isTracking;
                //language = cc.language;
                //lastModified = cc.lastModified;
                //mediaGridOffset = cc.mediaGridOffset;
                //mediaType = cc.mediaType;
                //mimeType = cc.mimeType;
                //resolution = cc.resolution;
                //rotationX = cc.rotationX;
                //rotationY = cc.rotationY;
                //rotationZ = cc.rotationZ;
                //scaleX = cc.scaleX;
                //scaleY = cc.scaleY;
                //scaleZ = cc.scaleZ;
                //text = cc.text;
                //translationX = cc.translationX;
                //translationY = cc.translationY;
                //translationZ = cc.translationZ;
                //url = cc.url;
            }
        }
    }
}