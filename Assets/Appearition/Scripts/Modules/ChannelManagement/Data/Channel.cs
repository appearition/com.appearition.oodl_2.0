﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Channel.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.Common;

namespace Appearition.ChannelManagement
{
    /// <summary>
    /// Container of an Appearition Channel JSON ApiData model.
    /// </summary>
    [System.Serializable]
    public class Channel
    {
        //Variables
        public int channelId;
        public string name;
        public ChannelImage[] images;
        public Setting[] settings;

        public Channel()
        {
        }

        /// <summary>
        /// Copy Constructor    
        /// </summary>
        /// <param name="cc">C.</param>
        public Channel(Channel cc)
        {
            channelId = cc.channelId;
            name = cc.name;
            cc.images.CopyTo(images, 0);
            cc.settings.CopyTo(settings, 0);
        }
    }
}