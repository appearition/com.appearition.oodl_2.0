﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    public class ArDemoUIHandler : MonoBehaviour
    {
        bool isFirstRun = true;
        //References
        //[SerializeField] Text _loadingText;
        public GameObject countdown;
        [SerializeField] List<GameObject> _uiToHideDuringLoading;
        [SerializeField] GameObject _scanAgainButton;
        [SerializeField] Text _scanAgainButtonText;
        public GameObject exitButton;
        public GameObject logoAnim;

        //Internal Variables
        public string scanAgainButtonIdleText = "Scan Again";
        public string scanAgainScanningText = "Scanning..";

        void Start()
        {
            //Bind to events
            AppearitionArHandler.OnInitializationComplete += AppearitionArHandler_OnInitializationComplete;
            AppearitionArHandler.OnScanStateChanged += AppearitionArHandlerOnScanStateChanged;

            //Hide the menus and show the loading text until the initialization is complete.
            //countdown.gameObject.SetActive(true);
            for (int i = 0; i < _uiToHideDuringLoading.Count; i++)
            {
                if (_uiToHideDuringLoading[i] != null)
                    _uiToHideDuringLoading[i].SetActive(false);
            }
        }

        void OnDestroy()
        {
            AppearitionArHandler.OnInitializationComplete -= AppearitionArHandler_OnInitializationComplete;
            AppearitionArHandler.OnScanStateChanged -= AppearitionArHandlerOnScanStateChanged;
        }

        /// <summary>
        /// Occurs when the initialization has is complete. Hides the loading text and shows the rest of the UI.
        /// </summary>
        private void AppearitionArHandler_OnInitializationComplete()
        {
            for (int i = 0; i < _uiToHideDuringLoading.Count; i++)
            {
                if (_uiToHideDuringLoading[i] != null)
                    _uiToHideDuringLoading[i].SetActive(true);
            }
        }

        private void AppearitionArHandlerOnScanStateChanged(bool isScanning)
        {
            if(isScanning)
            {
                countdown.SetActive(true);
                countdown.GetComponent<Animator>().Play("CountdownAnimation", -1, 0f);
            }
            if(!isScanning)
            {
                countdown.SetActive(false);
            }
            Debug.Log(isScanning);
            _scanAgainButton.SetActive(!isScanning);
            logoAnim.SetActive(isScanning);
        }

        /// <summary>
        /// Called by the Scan Again UI button. Turns on the Ar Provider's recognition to be scanning.
        /// The main reason why it shouldn't be constantly scanning is because it is a good practice to have a bit of control over the requests sent to Vuforia. There is no need to be scanning a target once it's either already being tracked, or when the user isn't even trying to scan.
        /// </summary>
        public void OnScanAgainButtonPressed()
        {
            AppearitionArHandler.Instance.ChangeArProviderScanningState(true);
        }

        /// <summary>
        /// Called by the UI toggle. Toggles the Ar Provider's active state: turn on or turn off the Ar Provider as a whole.
        /// It is recommended to have the Ar Provider off while you are expecting your users to not be able to scan; AR takes a lot of performance, heats up the device and consumes battery quite a fair bit.
        /// </summary>
        /// <param name="state"></param>
        public void OnToggleArProviderStatePressed(bool state)
        {
            AppearitionArHandler.Instance.ChangeArProviderActiveState(state);
        }
    }
}